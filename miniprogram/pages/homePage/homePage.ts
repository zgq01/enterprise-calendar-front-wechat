// index.ts
// 获取应用实例
Component({
  data: {
    
  },
  methods: {
    volist(value1){
      wx.request({
        url: 'http://localhost:8080/jeecg-boot/eScheduledType/volist',
        data: {
          token:value1
        },
        method:"GET",
        success:function(res){ 
          console.log(res);        
        }
      })
    
    },
    jumpTo(){
      // wx.navigateTo({   
      //   url: '../personCenter/personCenter',
      // })
      wx.login({  
        success (res) {
          console.log(res);     
          if (res.code) {
            //发起网络请求
            wx.request({
              url: 'http://localhost:8080/jeecg-boot/wx/login',
              data: {
                code:res.code
              },
              method:"POST",
              success:function(res){ 
                console.log(res);
                wx.setStorageSync('token', res.data.result.token);
                wx.request({
                  url: 'http://localhost:8080/jeecg-boot/wx/eScheduledType/volist',
                  header:{
                    token:res.data.result.token
                  },
                  method:"GET",
                  success:function(res){ 
                    console.log(res);        
                  }
                })
              }
            })
          } else {
            console.log('登录失败！' + res.errMsg)
          }
        }
      })
    },
  },
})
