Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    taskList: [
      {
        id: 1,
        createTime: "2024-01-01 ",
        startTime: "2024-01-02至 ",
        endTime: "2024-01-03 ",
        content: "完成任务A",
        creater: "张三",
        department: "技术部",
        status: "已完成",
        priority: "高"
      },
      {
        id: 2,
        createTime: "2024-01-02至",
        startTime: "2024-01-03",
        endTime: "2024-01-04 ",
        content: "完成任务B",
        creater: "李四",
        department: "销售部",
        status: "未完成",
        priority: "中"
      }
    ],
    searchText: '', // 搜索框文本
    modalVisible: false, // 弹窗是否显示
    selectedTask: {} as Task, // 选中的任务对象
  },


  /**
   * 组件的方法列表
   */
  methods: {
    onSearchInput(event: any) {
      this.setData({
        searchText: event.detail.value
      });
    },

    onTaskCardTap(event: any) {
      const taskId = event.currentTarget.dataset.id;
      const selectedTask = this.data.taskList.find((task: Task) => task.id === taskId);
      this.setData({
        modalVisible: true,
        selectedTask,
      });
    },

    hideModal() {
      this.setData({
        modalVisible: false,
        selectedTask: {} as Task,
      });
    },

    // 添加编译任务方法
    navigateToCompilePage() {
      wx.navigateTo({
        url: '../assignedTask/compile', // 替换成你实际的编译任务页面路径
      });
    },
    

    // 添加撤回任务方法
    revokeTask() {
      // 执行撤回任务逻辑
      console.log("撤回任务");
      // 清空弹窗内容
      this.setData({
        selectedTask: {} as Task,
      });
    },
  }
})


interface Task {
  id: number; // 添加 id 字段
  createTime: string;
  startTime: string;
  endTime: string;
  content: string;
  creater: string;
  department: string; // 添加 department 字段
  status: string;
  priority: string;
}