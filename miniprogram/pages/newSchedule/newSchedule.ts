// pages/newSchedule/newSchedule.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arr:['黄小小','张三','李四','王五'],
    index:null,
    hasFile: false,
    items:[
      {
        name:'一般',
        value:'common'
      },
      {
        name:'重要',
        value:'importance'
      }
    ],
    id: 0,
    taskId: 0,
    userId: '',
    userName: '',
    scheduleType: '',
    remindTime: '',
    isRepeat: '',
    isWeekend: '',
    isHoliday:'',
    isComplete: '',
    state: '',
    delFlag: '',
    fileName: '',
    filePath:'' ,
    document: '',
    createBy: '',
    createTime: '',
    updateBy: '',
    updateTime: '',
    address: '',
    isOa: '',
    dingId:'',
    date1: '2023-11-10',
    date2:'2023-11-11',
    time1:'23:30',
    time2:'00:30',
    beginTime:'',
    endTime:'',
    title:'',
    content:'',
    level:''
  },
  onDate1Change: function (e:any) {
    this.setData({
      date1: e.detail.value
    })
  },
  onTime1Change: function (e:any) {
    this.setData({
      time1: e.detail.value
    })
  },
  onDate2Change: function (e:any) {
    this.setData({
      date2: e.detail.value
    })
  },
  onTime2Change: function (e:any) {
    this.setData({
      time2: e.detail.value
    })
  },
  onInputTitle(event: any) {
    this.setData({
      title: event.detail.value
    });
  },
  onInputContent(event: any) {
    this.setData({
      content: event.detail.value
    });
  },
  // 单选按钮组变化事件处理函数
  radioChange(event: any) {
    let level = ''
    if (event.detail.value === 'common') {
       level = '一般';
    } else if (event.detail.value === 'importance') {
      level = '重要';
    }
    this.setData({
      level: level
    });
  },
   // 点击保存按钮事件处理函数
   saveData() {
    // 获取表单中的值
    const { beginTime,endTime,title,content,level,id,taskId,userId,userName,
      scheduleType,remindTime,isRepeat,isWeekend,isHoliday,isComplete,state,
      delFlag,fileName,filePath,document,createBy,createTime,updateBy,updateTime,address,isOa, dingId } = this.data;

    // 构造对象
    const dataObject = {
      beginTime: beginTime,
      endTime: endTime,
      title: title,
      content: content,
      level: level,
      id: id,
      taskId: taskId,
      userId: userId,
      userName: userName,
      scheduleType:scheduleType,
      remindTime: remindTime,
      isRepeat: isRepeat,
      isWeekend:isWeekend,
      isHoliday:isHoliday,
      isComplete: isComplete,
      state: state,
      delFlag: delFlag,
      fileName: fileName,
      filePath:filePath,
      document: document,
      createBy: createBy,
      createTime: createTime,
      updateBy: updateBy,
      updateTime: updateTime,
      address: address,
      isOa: isOa,
      dingId:dingId
    };
    // 打印对象
    console.log(dataObject);
     wx.request({
      url: `https://localhost:8080/jeecg-boot/calendar/rlSchedule/addSchedule`,
      method: 'POST',
      data:dataObject,
      success: (res) => {
        console.log('请求成功',res.data)
      },
      fail:(err)=>{
        console.log('请求失败',err)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    var beginTime = this.data.date1 + ' ' + this.data.time1;
    var endTime = this.data.date2 + ' ' + this.data.time2;
    this.setData({
      beginTime: beginTime,
      endTime: endTime
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})