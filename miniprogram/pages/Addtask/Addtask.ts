// pages/Addtask/Addtask.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arr:['黄小小','张三','李四','王五'],
    index:null,
    hasFile: false,
    path:'',
    filename:'',
    items:[
      {
        name:'common',
        value:'一般'
      },
      {
        name:'importance',
        value:'重要'
      }
    ],
    date1: '2023-11-10',
    date2:'2023-11-11',
    time1:'23:30',
    time2:'00:30'
  },
  onDate1Change: function (e:any) {
    this.setData({
      date1: e.detail.value
    })
  },
  onTime1Change: function (e:any) {
    this.setData({
      time1: e.detail.value
    })
  },
  onDate2Change: function (e:any) {
    this.setData({
      date2: e.detail.value
    })
  },
  onTime2Change: function (e:any) {
    this.setData({
      time2: e.detail.value
    })
  },
  radioChange:function(e:any){
    //获取单选数据
    console.log(e.detail.value)//输出的是{{item.name}}的值
  },
  pickerChange(value:any) {
    this.setData({
      index:value.detail.value
    })
  },
  uploadFileTap:function(e:any){
    let that = this;
    let myId = that.data.myId; 
    wx.chooseMessageFile({
        count: 1,
        type: 'file',
        success (res) {
          // tempFilePath可以作为img标签的src属性显示图片
          const tempFilePaths = res.tempFiles
          wx.uploadFile({
            url: url,//服务器上的pdf地址 
            filePath: tempFilePaths[0].path,
            name: 'file',
            formData: {
                file: tempFilePaths[0].path,
                schedulingId:myId
            },
            success (res){
              const data = res.data
              //do something
              wx.showToast({
                title: '数据上传成功！',
                icon: 'none',
                duration: 3000
                })
            }
          })
        }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})