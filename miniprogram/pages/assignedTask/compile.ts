import * as dateTimePicker from '../assignedTask/dateTimePicker.js';
import {
  MyModel
} from "../assignedTask/upLoad";
const my = new MyModel();

Component({
  /**
   * 组件的属性列表
   */
  properties: {},
  
  /**
   * 组件的初始数据
   */
  data: {
    names: ["张三", "李四", "王五"],
    selectedName: "", // 初始化时不选中任何人
    options: [
      { name: "一般", checked: false },
      { name: "重要", checked: false },
    ],
    date: '2018-10-01',
    time: '12:00',
    dateTimeArray: null,
    dateTime: null,
    dateTimeArray1: null,
    dateTime1: null,
    startYear: 2000,
    endYear: 2050
  },
  
  /**
   * 组件的方法列表
   */
  methods: {
    showNames() {
      wx.showActionSheet({
        itemList: this.data.names,
        success: (res) => {
          const selectedName = this.data.names[res.tapIndex];
          this.setData({
            selectedName,
          });
        },
      });
    },
    onRadioChange(event: any) {
      const value = parseInt(event.detail.value);
      const options = this.data.options.map((option, index) => {
        option.checked = index === value;
        return option;
      });
      this.setData({
        options,
      });
    },
    onInput(event: any) {
      this.setData({
        currentDate: event.detail,
      });
    },
    onLoad(this: any) {
      // 获取完整的年月日 时分秒，以及默认显示的数组
      var obj = dateTimePicker.dateTimePicker(this.data.startYear, this.data.endYear);
      var obj1 = dateTimePicker.dateTimePicker(this.data.startYear, this.data.endYear);
      
  
      this.setData({
        dateTime: obj.dateTime,
        dateTimeArray: obj.dateTimeArray,
        dateTimeArray1: obj1.dateTimeArray,
        dateTime1: obj1.dateTime
      });
    },
    changeDate(this: any, e: any) {
      this.setData({ date: e.detail.value });
    },
    changeTime(this: any, e: any) {
      this.setData({ time: e.detail.value });
    },
    changeDateTime(this: any, e: any) {
      this.setData({ dateTime: e.detail.value });
    },
    changeDateTime1(this: any, e: any) {
      this.setData({ dateTime1: e.detail.value });
    },
    changeDateTimeColumn(this: any, e: any) {
      var arr = this.data.dateTime, dateArr = this.data.dateTimeArray;
  
      arr[e.detail.column] = e.detail.value;
      dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);
  
      this.setData({
        dateTimeArray: dateArr,
        dateTime: arr
      });
    },
    changeDateTimeColumn1(this: any, e: any) {
      var arr = this.data.dateTime1, dateArr = this.data.dateTimeArray1;
  
      arr[e.detail.column] = e.detail.value;
      dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);
  
      this.setData({
        dateTimeArray1: dateArr,
        dateTime1: arr
      });
    },
    uploadFile: function (e:any) {
      var that = this;
      console.log("点击上传文件");
      wx.chooseMessageFile({
        count: 1,
        type: "file",
        success: function (res) {
          // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
          var tempFilePaths = res.tempFiles;
          my.uploadFile(that, tempFilePaths, 1);
        }
      })
    }
  
  },
});
